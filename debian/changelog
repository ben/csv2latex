csv2latex (0.23.1-1) unstable; urgency=medium

  * New upstream release 0.23.1 (Makefile changes)
  * Fix debian/rules

 -- Benoît Rouits <brouits@free.fr>  Fri, 07 Oct 2022 18:52:22 +0200

csv2latex (0.23-1) unstable; urgency=medium

  * New upstream release 0.23 (improve stdin reading)
  * Update debian/watch (version=4)
  * Avoid usage of cdbs
  * Remove debian/compat
  * Update debian/control
  * Add debian/upstream/metadata
  * Set Standards-version to 4.6.1

 -- Benoît Rouits <brouits@free.fr>  Thu, 06 Oct 2022 16:17:01 +0200

csv2latex (0.22-1) unstable; urgency=medium

  * new upstream release 0.22 (fix cross-compilation)
  * bump Standards-Version to 4.4.0

 -- Benoît Rouits <brouits@free.fr>  Sat, 04 Jan 2020 05:28:13 +0100

csv2latex (0.21-1) unstable; urgency=medium

  * new upstream version 0.21 (options --font and --landscape)

 -- Benoît Rouits <brouits@free.fr>  Mon, 02 Jul 2018 23:52:26 +0200

csv2latex (0.20-2) unstable; urgency=low

  * update Vcs-Svn and Vcs-Browser URLs in control file.

 -- Benoît Rouits <brouits@free.fr>  Thu, 15 Jun 2017 21:54:41 +0200

csv2latex (0.20-1) unstable; urgency=medium

  * New upstream release: fix separator guess on non-ascii chars, Makefile fix.

 -- Benoît Rouits <brouits@free.fr>  Sun, 30 Oct 2016 20:30:30 +0100

csv2latex (0.19-1) unstable; urgency=medium

  * New upstream release, support read from stdin.
  * Bump to Standards-Version 3.9.8
  * Minor fix on URIs
  * Use debhelper 10

 -- Benoît Rouits <brouits@free.fr>  Mon, 24 Oct 2016 02:01:42 +0200

csv2latex (0.18.1-1) unstable; urgency=low

  * new upstream (cosmetic) version
  * changed upstream URIs
  * fix lintian warnings, adapt to std 3.9.5

 -- Benoît Rouits <brouits@free.fr>  Tue, 29 Apr 2014 23:06:10 +0200

csv2latex (0.18-2) unstable; urgency=low

  * Switch to dpkg-source 3.0 (quilt) format
  * Fixed date warning in previous changelog entry

 -- Benoît Rouits <brouits@free.fr>  Tue, 27 Apr 2010 16:28:33 +0200

csv2latex (0.18-1) unstable; urgency=low

  * new upstream version

 -- Benoît Rouits <brouits@free.fr>  Tue, 27 Apr 2010 16:22:55 +0200

csv2latex (0.17-1) unstable; urgency=low

  * new upstream version

 -- Benoît Rouits <brouits@free.fr>  Sat, 06 Feb 2010 20:57:36 +0100

csv2latex (0.16-2) unstable; urgency=low

  * fixed maintainer lines in copyright/contol

 -- Benoît Rouits (package signature) <brouits@free.fr>  Mon, 26 Oct 2009 23:51:32 +0200

csv2latex (0.16-1) unstable; urgency=low

  * new upstream version

 -- Benoît Rouits (package signature) <brouits@free.fr>  Fri, 18 Sep 2009 23:40:43 +0200

csv2latex (0.15-1) unstable; urgency=low

  * option longtable added, sync'ed from upstream
  * fixes in debian/copyright and changelog

 -- Benoît Rouits (package signature) <brouits@free.fr>  Wed, 15 Jul 2009 16:26:00 +0200

csv2latex (0.14-2) unstable; urgency=low

  * debian/control: Add Vcs-Git and Vcs-Browser fields
  * debian/copyright: Change to the new machine-readable format, as in
    DEP5 (http://dep.debian.net/deps/dep5/)
  * debian/README.Debian: Remove file

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 17 Apr 2009 10:16:41 +0200

csv2latex (0.14-1) unstable; urgency=low

  * New upstream release
  * debian/control: Bump Standards-Version to 3.8.1 (no changes needed)

 -- Rafael Laboissiere <rafael@debian.org>  Thu, 16 Apr 2009 22:55:34 +0200

csv2latex (0.13-1) unstable; urgency=low

  * New upstream release
  * debian/watch: Add URL for upstream tarballs

 -- Rafael Laboissiere <rafael@debian.org>  Thu, 12 Mar 2009 02:24:54 +0100

csv2latex (0.12-4) unstable; urgency=low

  * First official release in Debian (closes: #518850)
  * debian/rules: Switch to CDBS
  * debian/control:
    + Add myself to the Uploaders list
    + Set Architecture to any
    + Bump Standards-Version to 3.8.0 (no changes needed)
    + Add Homepage field and remove the URL from the long description
    + Build-depend on cdbs
    + Fix Lintian warning debhelper-but-no-misc-depends
    + Build-depend on debhelper >= 7
  * debian/compat: Bump the debhelper compatibility level to 7
  * debian/watch: Add an empty watch file as a hint to the Debian External
    Health Status (Lintian warning debian-watch-file-is-missing)
  * debian/copyright: Add Copyright notice to the license terms

 -- Rafael Laboissiere <rafael@debian.org>  Tue, 10 Mar 2009 16:20:05 +0100

csv2latex (0.12-3) UNRELEASED; urgency=low

  * now, --lines 0 means no limit per output table (actually 2 Giga lines)

 -- Benoît Rouits <brouits@free.fr>  Thu, 29 Jan 2009 16:04:00 +0100

csv2latex (0.12-2) UNRELEASED; urgency=low

  * manpage added (!) sgml version in debian/manpage.sgml

 -- Benoît Rouits <brouits@free.fr>  Sat, 24 Jan 2009 01:32:00 +0100

csv2latex (0.12-1) UNRELEASED; urgency=low

  * minor bugfix (getMaximums) in case of unexpected EOF
  * --version looks nicer

 -- Benoît Rouits <brouits@free.fr>  Sat, 24 Jan 2009 01:32:00 +0100

csv2latex (0.12) UNRELEASED; urgency=low

  * added --reduce option, based on the idea from Boaz (but need {relsize})

 -- Benoît Rouits <brouits@free.fr>  Fri, 23 Jan 2009 19:50:00 +0100

csv2latex (0.11) UNRELEASED; urgency=low

  * bugfix a bad behaviour when getting a separator inside a block
  * rewritten some parsing code

 -- Benoît Rouits <brouits@free.fr>  Thu, 22 Jan 2009 23:50:00 +0100

csv2latex (0.10-1) UNRELEASED; urgency=low

  * Added co(l)on as --delimiter. Modified Usage message. Better --guess

 -- Benoît Rouits <brouits@free.fr>  Tue, 11 Nov 2008 23:50:00 +0100

csv2latex (0.9-3) UNRELEASED; urgency=low

  * Added s(p)ace as --delimiter. Added --colorrows option

 -- Jason J. Corso <jcorso@cse.Buffalo.EDU>  Thu, 29 Nov 2007 18:20:00 +0100

csv2latex (0.9-2) UNRELEASED; urgency=low

  * Added bad filename detection (bug report by James_R_Phillips@bigfoot.com)

 -- Benoît Rouits <brouits@free.fr>  Tue, 17 Apr 2007 14:26:30 +0100

csv2latex (0.9-1) UNRELEASED; urgency=low

  * Added "--separator t" option by Owen Solberg <solberg@berkeley.edu>

 -- Benoît Rouits <brouits@free.fr>  Sat, 04 Mar 2006 20:21:53 +0100

csv2latex (0.8-1) UNRELEASED; urgency=low

  * Initial release

 -- Benoît Rouits <brouits@free.fr>  Tue, 22 Nov 2005 15:41:17 +0100
