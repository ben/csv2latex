.TH "CSV2LATEX" "1" 
.SH "NAME" 
csv2latex \(em convert a csv file into a LaTeX document 
.SH "SYNOPSIS" 
.PP 
\fBcsv2latex\fR [\fB\-\-nohead\fP]  [\fB\-\-longtable\fP]  [\fB\-\-noescape\fP]  [\fB\-\-guess\fP]  [\fB\-\-separator \fIc\fR|\fIs\fR|\fIt\fR|\fIp\fR|\fIl\fR\fP]  [\fB\-\-block \fIq\fR|\fId\fR|\fIn\fR\fP]  [\fB\-\-lines \fI#\fR\fP]  [\fB\-\-position \fIl\fR|\fIc\fR|\fIr\fR\fP]  [\fB\-\-colorrows \fI0-1\fR\fP]  [\fB\-\-reduce \fI1\fR|\fI2\fR|\fI3\fR|\fI4\fR\fP]  [\fB\-\-repeatheader\fP]  [\fB\-\-nohlines\fP]  [\fB\-\-novlines\fP]  [\fB\-\-landscape\fP]  [\fB\-\-font \fI#\fR\fP]  [file]  
.SH "DESCRIPTION" 
.PP 
This manual page documents the \fBcsv2latex\fR program. 
.PP 
\fBcsv2latex\fR is a program that reads a "comma separated values" (csv) file 
and outputs a LaTeX file with one or more tabular environments to display 
the printable values of the csv file. The LaTeX code is flushed on the standard output. 
 
.PP 
So-called "comma separated values" files are common formats for exchanging two-dimensinal 
tables between programs such as spreadsheets editors, to represent almost any kind of data. 
By default, a csv file is made of printable data separated by commas (`,'), each comma 
representing a `cell' separator, and each line representing a row. By extension, cell  
separators can be represented by tabs if the comma is considered as printable data. 
Moreover, some non true csv files can be assumed as two-dimensional tables as well. 
In some circumstances, if the printable data includes the cell separator of the 
exchange format, the latter can use a second extra character to embrace the printable 
data into a block (e.g: quoted text). Thus, it is still possible to parse the file by 
using the block delimiter (used twice to embrace the cell) instead of the separator. 
 
.PP 
\fBcsv2latex\fR aims to parse various csv formats plus formats that fits 
into the above definiton, assuming the data is text, and to produce a yet simple LaTeX file 
using the "tabular" environment for a table-style layout. 
Some options of output will also use macros provided by extra 
LaTeX packages that are commonly included in the main LaTeX distributions. 
 
.SH "OPTIONS" 
.PP 
This program follows the usual GNU command line syntax, 
with long options starting with two dashes (`\-').  A summary of 
options is included below. 
.IP "\fB-h\fP           \fB\-\-help\fP         " 10 
Show summary of options. 
.IP "\fB-v\fP           \fB\-\-version\fP         " 10 
Show version of program. 
.IP "\fB-n\fP           \fB\-\-nohead\fP         " 10 
Do not output the LaTeX document header.  
This is useful when the output is to be included as a separate file into the master document. 
 
.IP "\fB-t\fP           \fB\-\-longtable\fP         " 10 
uses the 'longtable' package instead of the 'tabular' one. 
This is useful when the input is long, with \fB\-\-lines 0\fP option. 
This option uses the extra `longtable' LaTeX package. 
If you also use \fB\-\-nohead\fP option, do not forget to add  
the following line into the header of your master document: "\\usepackage{longtable}". 
 
.IP "\fB-x\fP           \fB\-\-noescape\fP         " 10 
Do not escape TeX control characters from the input.  
This is useful when the input contains already TeX code. 
 
.IP "\fB-g\fP           \fB\-\-guess\fP         " 10 
Try to guess the csv format. 
This is useful when the input is not strictly a comma separated set of printable data. 
For example, a line like %Foo, Bar%:%Wizz: Hey% may be parsed as "Foo, Bar" then "Wizz: Hey". 
 
.IP "\fB-s \fIc\fR|\fIs\fR|\fIt\fR|\fIp\fR|\fIl\fR\fP           \fB\-\-separator \fIc\fR|\fIs\fR|\fIt\fR|\fIp\fR|\fIl\fR\fP         " 10 
Set the given separator as cell separator of the csv format. 
`c' means a comma (default). 
`s' means a semicolon. 
`t' means a tab. 
`p' means a space. 
`l' means a colon. 
 
.IP "\fB-b \fIq\fR|\fId\fR|\fIn\fR\fP           \fB\-\-block \fIq\fR|\fId\fR|\fIn\fR\fP         " 10 
Set the given block delimiter that embraces the printable data of the csv format. 
`q' means a simple quote. 
`d' means a double quote. 
`n' means no quoting at all (default). 
 
.IP "\fB-l \fI#\fR\fP           \fB\-\-lines \fI#\fR\fP         " 10 
Force to output multiple tabulars, each having a limited number of lines. 
The given argument must be a POSITIVE INTEGER VALUE. This is useful when 
the number of input rows is too big to fit into a single papersheet.  
A good average for a4 paper is about 40 lines (default). 0 means infinity 
(actually about 2 Giga lines). 
 
.IP "\fB-p \fIl\fR|\fIc\fR|\fIr\fR\fP           \fB\-\-position \fIl\fR|\fIc\fR|\fIr\fR\fP         " 10 
Set the text position in all cells at once. 
This simply uses one of the three basic cell formatting options of the LaTeX tabular environment. 
`l' means left-aligned (default). 
`c' means centered. 
`r' means right-aligned. 
 
.IP "\fB-c \fI0-1\fR\fP           \fB\-\-colorrows \fI0-1\fR\fP         " 10 
Alternate white/gray rows on the LaTeX output, having the given graylevel. 
The given argument must be a REAL NUMBER BETWEEN 0 AND 1.  
0 means black while 1 means white. 
A nice looking value is 0.75 when printed on white paper. 
This option uses the extra `colortbl' LaTeX package. 
If you also use \fB\-\-nohead\fP option, do not forget to add  
the following line into the header of your master document: "\\usepackage{colortbl}". 
 
.IP "\fB-r \fI1\fR|\fI2\fR|\fI3\fR|\fI4\fR\fP           \fB\-\-reduce \fI1\fR|\fI2\fR|\fI3\fR|\fI4\fR\fP         " 10 
Reduce the size of the tabular and the font in the LaTeX output, given a reduction level. 
The given argument must be one of 1, 2, 3 or 4. 
The more the level is high, the more the tabular will appear small. 
This is useful to shrink the table width when the printable data is made of very long text. 
This option uses the extra `relsize' LaTeX package. 
If you also use \fB\-\-nohead\fP option, do not forget to add  
the following line into the header of your master document: "\\usepackage{relsize}". 
 
.IP "\fB-z\fP           \fB\-\-nohlines\fP         " 10 
Do not output horizontal lines in the table(s). 
 
.IP "\fB-y\fP           \fB\-\-novlines\fP         " 10 
Do not output vertical lines in the table(s). 
 
.IP "\fB-e\fP           \fB\-\-repeatheader\fP         " 10 
Repeat the first row of the first table in every table.  
This is useful when the output is very long and separated in 
multiple tables. 
 
.IP "\fB-f \fI#\fR\fP           \fB\-\-font \fI#\fR\fP         " 10 
Set the font size to be inserted on the header and use it to compute lines per table. 
The given argument must be an integer in points unit. 
If used in conjunction with \fB\-\-lines\fP set to 0, then the lines per tabular 
is computed based on LaTeX \\textheight defaulting to 592 pt. 
If the \fB\-\-longtable\fP is used, then the computation is not done. 
If you also use \fB\-\-nohead\fP option, you should use the same font size 
according to your own LaTeX document for best result. 
 
.IP "\fB-a\fP           \fB\-\-landscape\fP         " 10 
Set landscape mode in the LaTeX document header. If used in conjunction with 
\fB\-\-lines\fP set to 0 and \fB\-\-font\fP size, then the \\textheight used 
to compute lines per tabular defaults to (592 / 1.414) pt. If you also use \fB\-\-nohead\fP 		option, you should set your own LaTeX document to landscape mode for best result. 
 
.SH "EXAMPLES" 
.PP 
Create a PDF document with small text, alternate gray rows, 80 lines per table, 
from a guessed csv format of the january stats that my boss created with his  
super point-and-click spreadsheet program (which could not generate a PDF output!). 
 
.PP 
\fBcsv2latex \-\-guess \-\-lines 80 \-\-colorrows 0.75 \-\-reduce 2 january_stats.csv > january_stats.tex && pdflatex january_stats.tex\fR      
.PP 
Quickly preview a phonebook from a file formatted as "Surname" "Name" "Phone" "Cellular": 
 
.PP 
\fBcsv2latex \-s p \-b d \-l 42 phonebook-sorted.txt | latex\fR      
.SH "SEE ALSO" 
.PP 
tex (1), latex (1). 
.\" created by instant / docbook-to-man 
