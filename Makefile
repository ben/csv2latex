PROGNAME=csv2latex
PREFIX=/usr/local
CFLAGS?=-Wall -Wextra -pedantic
INSTALL=install
BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/share/man/man1
GZIP=gzip --to-stdout
TEST=$(shell wc -l < ./test.csv)
all:
	$(CC) $(CPPFLAGS) $(CFLAGS) $(PROGNAME).c $(LDFLAGS) -o $(PROGNAME)
	$(GZIP) $(PROGNAME).1 > $(PROGNAME).1.gz
install:
	$(INSTALL) $(PROGNAME) ${DESTDIR}$(BINDIR)/$(PROGNAME)
	$(INSTALL) -D $(PROGNAME).1.gz ${DESTDIR}$(MANDIR)/$(PROGNAME).1.gz
uninstall:
	$(RM) ${DESTDIR}$(BINDIR)/$(PROGNAME)
	$(RM) ${DESTDIR}$(MANDIR)/$(PROGNAME).1.gz
clean:
	$(RM) $(PROGNAME)
	$(RM) $(PROGNAME).1.gz
test: $(PROGNAME)
	@test `./csv2latex test.csv | grep -Fi fourth | wc -l` = $(TEST) && echo Test 1 OK. || echo Test 1 KO!
	@test `./csv2latex < test.csv | grep -Fi fourth | wc -l` = $(TEST) && echo Test 2 OK. || echo Test 2 KO!
	@test `cat ./test.csv | ./csv2latex | grep -Fi fourth | wc -l` = $(TEST) && echo Test 3 OK. || echo Test 3 KO!
# vim:set ts=8 sw=8 noexpandtab et:
